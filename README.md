# [Enfoke Technical Test] - Rick & Morty Characters

## Url de Deploy
https://ramc.netlify.app/

Screenshot
![image description](/src/assets/screenshot.webp)

## Enunciado

- [x] Crear aplicación React + Vite + Typescript.
- [x] Implementar Ant Design como framework de UI .
- [x] Al iniciar la app, se debe consumir el endpoint https://rickandmortyapi.com/api/character
- [x] Mostrar en una tabla paginada, las siguientes propiedades de cada objeto devuelto:
  - Name
  - Status
- [x] Además, la tabla debe tener una columna llamada "Actions". En dicha columna, por cada registro, debe haber un ícono -que al hacer click- muestre un popup con más información de cada personaje:
  - Name
  - Status
  - Gender
  - Location => Name
  - Un texto que indique:
    - "Estuvo en un solo episodio"
    - "Estuvo en más de un episodio" (Sin especificar el número exacto).

Para este popup, se puede usar el/los componentes de Ant Design que el desarrollador crea adecuado.
- [x] Crear repo en Gitlab para versionar el proyecto.
- [x] Ante cada commit en la rama main, se debe ejecutar un pipeline automático qué, de la forma que el desarrollador desee, haga un deploy automático a un sitio público.
Como alternativa, si lo anterior se complica, se puede optar por crear un pipeline automático que, ante cada commit a la rama main, ejecute dos jobs:
  - Un primer job que realice la compilación en Vite
  - Un segundo job que lleve la compilación del job anterior a una imagen Docker basada en nginx y la suba al Gitlab Container Registry.

  El objetivo de este segundo job es que quien tenga acceso a la imagen Docker generada, pueda crear fácilmente un contenedor para usar la aplicación. Si se opta por este camino, no hace falta hacer el deploy de la misma.

### Agregados (fuera del enunciado original)

- [x] Setup inicial
  - Agregar reglas de Eslint
  - Instalar y configurar Husky, lint staged, y commitlint
  - Agregar alias (src)
  - Agregar template de merge request

- [x] Styling en general

## ¿Qué se evalúa?
- Calidad de código
- Arquitectura
- Capacidad de investigación (Para lo que es deploys automáticos o generación automática de la imagen Docker)
- Cumplimiento de los requerimientos en tiempo y forma.
