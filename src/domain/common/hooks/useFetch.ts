import { useEffect, useState } from 'react'
import { apiGet } from '~/lib'

interface Props {
  url: string,
  // TODO: set params for example to filter characters
}

interface Fetch<T> {
  isLoading: boolean
  error: string | null
  data: T | null
}

const handleError = (e: unknown): string => {
  if (typeof e === 'string') return e
  if (e instanceof Error) return e?.message
  return 'Unexpected Error'
}

export const useFetch = <T>({ url }: Props): Fetch<T> => {
  
  const [apiCall, setApiCall] = useState<Fetch<T>>({
    isLoading: true,
    error: null,
    data: null
  })

  useEffect(() => {
    const executeApiFetch = async () => {
      try {
        const response = await apiGet(url)
        const isLoading = false
        if (response?.status !== 200) {
          setApiCall({ 
            isLoading, 
            data: null,
            error: `Error: ${ response?.status } - ${ response?.statusText }`
          })
          return
        }
        setApiCall({
          isLoading,
          error: null,
          data: response?.data
        })
      } catch (e: unknown) {
        const error = handleError(e)
        setApiCall({
          isLoading: false,
          data: null,
          error
        })
      }
    }

    executeApiFetch()
  }, [url])

  return apiCall
}
