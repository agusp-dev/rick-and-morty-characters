import { Button } from 'antd'
import { EyeFilled } from '@ant-design/icons'
import type { CharacterID } from '~/types'

interface Props {
  id: CharacterID
  handleClick: (id: CharacterID) => void
}

export const ShowCharacterButton: React.FC<Props> = ({ id, handleClick }) => {

  const onHandleClick = (id: CharacterID) => {
    handleClick(id)
  }

  return (
    <Button 
      shape='circle' 
      icon={ <EyeFilled /> }
      onClick={ () => onHandleClick(id) } 
    />
  )
}
