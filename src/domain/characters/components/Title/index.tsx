import { Flex, Typography } from 'antd'

export const Title = () => (
  <Flex style={{ margin: '16px' }} justify='center' align='end' gap='12px'>
    <Typography.Title 
    style={{ 
        margin: 0,
        textAlign: 'center',
      }}
    >
      Rick & Morty Characters
    </Typography.Title>
    <Typography.Text style={{ paddingBottom: '6px', color: 'gray' }}>(V1.0)</Typography.Text>
  </Flex>
)
