import { Flex, Avatar, Typography } from 'antd'

type Props = {
  name: string,
  url: string
}

export const TableCharacterName: React.FC<Props> = ({ name, url }) => (
  <Flex align='center' gap='16px'>
    <Avatar src={ <img src={ url } alt={ name } /> } />
    <Typography.Text 
      strong 
      style={{ fontSize: '16px' }}
    >
      { name }
    </Typography.Text>
  </Flex>
)
