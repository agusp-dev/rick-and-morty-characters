import { Tag } from 'antd'
import { CheckCircleOutlined, ExclamationCircleOutlined, MinusCircleOutlined } from '@ant-design/icons'
import { CHARACTER_STATUS, COLOR } from './status'
import type { StatusColor, StatusIcon } from './types'

const ICON: StatusIcon = {
  [CHARACTER_STATUS.ALIVE]: <CheckCircleOutlined />,
  [CHARACTER_STATUS.DEAD]: <MinusCircleOutlined />,
  [CHARACTER_STATUS.UNKNOWN]: <ExclamationCircleOutlined />
}

type Props = {
  status: string
  fontSize?: string
  padding?: string
}

export const TableCharacterStatus: React.FC<Props> = ({ status, fontSize, padding }) => {

  const characterStatus = status?.toLowerCase()

  return (
    <Tag 
      style={{ padding: padding ?? '1px 6px', fontSize: fontSize ?? '14px' }}
      color={ COLOR[characterStatus as keyof StatusColor] }
      icon={ ICON[characterStatus as keyof StatusIcon] } 
    >
      { status }
    </Tag>
  )
}
