import type { StatusColor } from './types'

export enum CHARACTER_STATUS {
  ALIVE = 'alive',
  DEAD = 'dead',
  UNKNOWN = 'unknown'
}

export const COLOR: StatusColor = {
  [CHARACTER_STATUS.ALIVE]: 'green',
  [CHARACTER_STATUS.DEAD]: 'red',
  [CHARACTER_STATUS.UNKNOWN]: 'default'
}
