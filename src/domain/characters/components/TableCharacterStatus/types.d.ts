export interface StatusColor {
  alive: string,
  dead: string,
  unknown: string
}

export interface StatusIcon {
  alive: React.ReactNode,
  dead: React.ReactNode,
  unknown: React.ReactNode,
}
