import { Table } from 'antd'
import { TableCharacterName } from '../TableCharacterName'
import { TableCharacterStatus } from '../TableCharacterStatus'
import { ShowCharacterButton } from '../ShowCharacterButton'
import type { CharacterID, CharacterTable as CharacterTableType } from '~/types'
import { memo } from 'react'

const EmptyState = () => (
  <p>No Data</p>
)

interface Props {
  list: CharacterTableType[]
  onShowCharacter: (id: CharacterID) => void
}

const Component: React.FC<Props> = ({ list, onShowCharacter }) => {

  const handleView = (id: CharacterID) => {
    onShowCharacter(id)
  }

  return list?.length 
    ? <Table rowKey='id' dataSource={ list }>
        <Table.Column 
          title='Name' 
          dataIndex='name' 
          key='name'
          render={(_: unknown, record: CharacterTableType) => (
            <TableCharacterName name={ record?.name } url={ record?.image } />
          )} 
        />
        <Table.Column 
          title='Status' 
          dataIndex='status' 
          key='status'
          render={(status) => <TableCharacterStatus status={ status } />}
        />
        <Table.Column 
          title='Actions' 
          dataIndex='actions' 
          key='actions' 
          align='right'
          render={(_: unknown, record: CharacterTableType) => (
            <ShowCharacterButton 
              id={ record?.id } 
              handleClick={ handleView } 
            />
          )} 
        />
      </Table>
    : <EmptyState />
}

export const CharacterTable = memo(Component)
