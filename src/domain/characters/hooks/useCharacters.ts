import { useFetch } from '../../common'
import type { Character } from '~/types'

const API_URL = 'https://rickandmortyapi.com/api/character'

interface Characters {
  isLoading: boolean
  error: string | null
  data: Character[] | null
}

interface CharactersApi {
  info: object
  results: Character[]
}

export const useCharacters = (): Characters => {
  const { isLoading, error, data } = useFetch<CharactersApi>({ url: API_URL })
  return {
    isLoading, 
    error,
    data: data?.results ?? null
  }
}
