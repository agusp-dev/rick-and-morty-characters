import { Character, CharacterTable } from '~/types'

type List = Character[] | null 
type TableCharacters = CharacterTable[]

export const mappedTableCharacters = (list: List ): TableCharacters => (
  list?.map(({
    id, 
    name, 
    status, 
    image 
  }) => ({ id, name, status, image })) ?? []
)
