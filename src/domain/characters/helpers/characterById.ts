import type { Character, CharacterID, CharacterDetails } from '~/types'

type CharacterById = CharacterDetails | undefined 

type List = Character[] | null

export const characterById = (list: List, cId: CharacterID): CharacterById => {
  const selected = list?.find(({ id }) => cId === id)
  if (!selected) return
  const { id, name, status, image, gender, location, episode } = selected
  return {
    id,
    name, 
    status,
    image, 
    gender,
    location, 
    episode
  }
} 