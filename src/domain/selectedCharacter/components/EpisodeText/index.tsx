import { Flex, Typography } from 'antd'

const ONE_EPISODE_TEXT = 'It was in only one episode'
const MANY_EPISODES_TEXT = 'Was in more than one episode'

interface Props {
  quantity: number
}

export const EpisodeText: React.FC<Props> = ({ quantity }) => (
  <Flex 
    vertical 
    align='center'
    gap='4px'
    style={{
      width: '100%',
      backgroundColor: '#f5f5f5', 
      borderRadius: '4px',
      padding: '12px',
      marginTop: '12px'
    }}
  >
    <Typography.Title 
      level={5}
      style={{
        margin: 0,
        fontSize: '12px',
        color: '#8c8c8c'
      }}
    >
      Episodes
    </Typography.Title>
    <Typography.Text style={{ fontSize: '16px' }}>
      { quantity > 1 ? MANY_EPISODES_TEXT : ONE_EPISODE_TEXT }
    </Typography.Text>
  </Flex>
)
