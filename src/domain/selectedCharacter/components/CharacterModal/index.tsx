import { Modal, Flex, Typography } from 'antd'
import { HeartOutlined, UserOutlined, EnvironmentOutlined } from '@ant-design/icons'
import { CharacterDetails } from '~/types'
import { CharacterDetail } from '../CharacterDetail'
import { EpisodeText } from '../EpisodeText'

interface Props {
  isOpen: boolean
  onClose: () => void
  character: CharacterDetails | undefined
}

export const CharacterModal: React.FC<Props> = ({ isOpen, onClose, character }) => (
  <Modal 
    width='340px'
    footer={null} 
    open={ isOpen }  
    onCancel={ onClose }
  >
    <Flex 
      vertical 
      align='center'
      style={{ padding: '24px 16px' }}
    >
      <img 
        style={{
          width: '160px',
          height: '160px',
          borderRadius: '50%',
        }}
        src={ character?.image } 
        alt={ character?.name } 
      />
      <Typography.Title level={ 3 } style={{ fontSize: '32px', margin: '20px 0' }}>
        { character?.name }
      </Typography.Title>
      <Flex vertical gap='16px' align='center' style={{ width: '100%' }}>
        <Flex gap='32px'>
          <CharacterDetail 
            Icon={ HeartOutlined } 
            title='Status'
            value={ character?.status || 'Unknown' } 
          />
          <CharacterDetail 
            Icon={ UserOutlined } 
            title='Gender'
            value={ character?.gender } 
          />
        </Flex>
        <CharacterDetail 
          Icon={ EnvironmentOutlined } 
          title='Location'
          value={ character?.location?.name } 
        />
        <EpisodeText quantity={ character?.episode?.length ?? 0 } />
      </Flex>
    </Flex>
  </Modal>
)
