import { Flex, Typography } from 'antd'
import { AntdIconProps } from '@ant-design/icons/lib/components/AntdIcon'

interface Props {
  Icon: React.ForwardRefExoticComponent<Omit<AntdIconProps, 'ref'>>
  title: string
  value: string | undefined
}

export const CharacterDetail: React.FC<Props> = ({ Icon, title, value }) => {
  return (
    <Flex vertical gap='2px' justify='center' align='center'>
      <Flex gap='8px'>
        <Icon style={{ fontSize: '14px', color: '#8c8c8c' }} />
        <Typography.Title 
          level={ 5 } 
          style={{ fontSize: '12px', color: '#8c8c8c', margin: 0 }}
        >
          { title }
        </Typography.Title>
      </Flex>
      <Typography.Text
        style={{ fontSize: '18px' }}
      >
        { value }
      </Typography.Text>
    </Flex>
  )
}