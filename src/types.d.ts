
interface Origin {
  name: string;
  url: string;
}

interface Location {
  name: string;
  url: string;
}

export type CharacterID = number
export interface Character {
  id: CharacterID
  name: string
  status: string
  species: string
  type: string
  gender: string
  origin: Origin
  location: Location
  image: string
  episode: string[]
  url: string
  created: string
}

export interface CharacterTable {
  id: Character['id']
  name: Character['name']
  status: Character['status']
  image: Character['image']
}

export interface CharacterDetails extends CharacterTable{
  gender: Character['gender']
  location: Character['location']
  episode: Character['episode']
}
