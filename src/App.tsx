import { CharactersPage } from '~/ui'

function App() {
  return (
    <CharactersPage />
  )
}

export default App
