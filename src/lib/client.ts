import axios from 'axios'

export const apiGet = async (url: string) => {
  try {
    const response = await axios.get(url)
    return response
  } catch (e: unknown) {
    if (typeof e === 'string') throw new Error(e)
    if (e instanceof Error) throw e
  }
}
