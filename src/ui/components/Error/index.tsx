import { Typography } from 'antd'

interface Props {
  text: string
}

export const Error: React.FC<Props> = ({ text }) => (
  <Typography.Text 
    style={{ 
      textAlign: 'center' 
    }}
  >
    Error: { text }
  </Typography.Text>
)
