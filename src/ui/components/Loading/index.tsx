import { Typography } from 'antd'

interface Props {
  text: string
}

export const Loading: React.FC<Props> = ({ text }) => (
  <Typography.Text 
    style={{ 
      textAlign: 'center' 
    }}
  >
    { text }
  </Typography.Text>
)
