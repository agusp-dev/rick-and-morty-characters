import { useCallback, useMemo, useState } from 'react'
import { 
  Title, 
  CharacterTable, 
  useCharacters, 
  mappedTableCharacters,
  characterById
} from '~/domain/characters'
import { CharacterModal } from '~/domain/selectedCharacter'
import type { CharacterID, CharacterDetails } from '~/types'
import { Loading, Error } from '~/ui/components'

type ModalOpen = boolean
type SelectedCharacter = CharacterDetails | undefined

export const CharactersPage = () => {

  const [isModalOpen, setIsModalOpen] = useState<ModalOpen>(false)
  const [selectedCharacter, setSelectedCharacter] = useState<SelectedCharacter>()

  const { isLoading, error, data } = useCharacters()

  const tableCharacters = useMemo(() => mappedTableCharacters(data), [data])

  const onHandleModalClose = () => {
    setSelectedCharacter(undefined)
    setIsModalOpen(false)
  }

  const onHandleModalOpen = useCallback((id: CharacterID) => {
    const selected = characterById(data, id)
    if (!selected) return
    setSelectedCharacter(selected)
    setIsModalOpen(true)
  }, [data])

  return (
    <>
      <Title />
      { isLoading && <Loading text='Loading characters...' /> }
      { error && <Error text={ error } /> }
      { !isLoading && !error && (
        <>
          <CharacterTable 
            list={ tableCharacters }
            onShowCharacter={ onHandleModalOpen } 
          />
          <CharacterModal 
            isOpen={ isModalOpen } 
            onClose={ onHandleModalClose } 
            character={ selectedCharacter }
          />
        </>
      )}
    </>
  )
}
